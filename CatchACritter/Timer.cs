﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace CatchACritter
{
    class Timer
    {
        // ---------------------------
        // Data
        // ---------------------------
        float timeRemaining = 0;
        Vector2 position = new Vector2(200, 10);
        SpriteFont font = null;
        bool running = false;

        // Delegates
        // The delegate definition states what type of functions are allowed for this delegate
        public delegate void TimeUp();
        public TimeUp ourTimerCallback;

        // ---------------------------
        // Behaviour
        // ---------------------------
        public void LoadContent(ContentManager content)
        {
            // Load the font from file so it can be used to draw text
            font = content.Load<SpriteFont>("fonts/mainFont");
        }
        // ---------------------------
        public void Draw(SpriteBatch spriteBatch)
        {
            // Draw the timer to the screen using the font variable
            int timeInt = (int)timeRemaining;
            spriteBatch.DrawString(
                font,
                "Time: " + timeInt.ToString(),
                position,
                Color.White);
        }
        // ---------------------------
        public void Update(GameTime gameTime)
        {
            if (running == true)
            {
                timeRemaining -= (float)gameTime.ElapsedGameTime.TotalSeconds;

                // If our time has run out...
                if (timeRemaining <= 0)
                {
                    // Stop. Do something.
                    running = false;
                    timeRemaining = 0;

                    // The something to do:
                    if (ourTimerCallback != null)
                        ourTimerCallback();
                }
            }
        }
        // ---------------------------
        public void StartTimer()
        {
            running = true;
        }
        // ---------------------------
        public void SetTimer(float newTime)
        {
            timeRemaining = newTime;
        }
        // ---------------------------

    }
}
