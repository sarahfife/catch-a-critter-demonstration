﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;

namespace CatchACritter
{
    class Button
    {
        // ---------------------------
        // Data
        // ---------------------------
        Texture2D image = null;
        Vector2 position = Vector2.Zero;
        SoundEffect click = null;
        bool visible = true;

        // Delegates
        // The delegate definition states what type of functions are allowed for this delegate
        public delegate void OnClick();
        public OnClick ourButtonCallback;

        // ---------------------------
        // Behaviour
        // ---------------------------
        public void LoadContent(ContentManager content)
        {
            image = content.Load<Texture2D>("graphics/button");
            click = content.Load<SoundEffect>("audio/buttonclick");
        }
        // ---------------------------
        public void Draw(SpriteBatch spriteBatch)
        {
            if (visible == true)
            {
                spriteBatch.Draw(image, position, Color.White);
            }
        }
        // ---------------------------
        public void Input()
        {
            // Get the current status of the mouse
            MouseState currentState = Mouse.GetState();

            // Get the bounding box for the critter
            Rectangle bounds = new Rectangle(
                (int)position.X,
                (int)position.Y,
                image.Width,
                image.Height);

            // Check if we have the left button held down over the button on screen
            // And if the button is visible
            if (currentState.LeftButton == ButtonState.Pressed
                && bounds.Contains(currentState.X, currentState.Y)
                && visible == true)
            {
                // We clicked the button!

                // Play SFX
                click.Play();

                // Hide the button
                visible = false;

                // Start the game
                if (ourButtonCallback != null)
                    ourButtonCallback();
            }
        }
        // ---------------------------
        public void Show()
        {
            visible = true;
        }
        // ---------------------------

    }
}
