﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;

namespace CatchACritter
{
    class Critter
    {
        // ---------------------------
        // Type Definitions
        // ---------------------------
        public enum Species
            // A special type of integer with only specific values allowed, that have names
        {
            CROCODILE,  // = 0
            HORSE,      // = 1
            ELEPHANT,   // = 2

            // --

            NUM         // = 3
        }


        // ---------------------------
        // Data
        // ---------------------------
        Texture2D image = null;
        Vector2 position = Vector2.Zero;
        bool alive = false;
        Score scoreObject = null;
        int critterValue = 10;
        SoundEffect click = null;
        Species ourSpecies = Species.CROCODILE;

        // ---------------------------
        // Behaviour
        // ---------------------------
        public Critter(Score newScore, Species newSpecies)
        {
            // Constructor. Called when the object is created.
            // No return type (special)
            // Helps decide how the object will be set up
            // Can have arguments (such as newScore)
            // This one lets us have access to the game's score.
            scoreObject = newScore;
            ourSpecies = newSpecies;
        }
        // ---------------------------
        public void LoadContent(ContentManager content)
        {

            switch(ourSpecies)
            {
                case Species.CROCODILE:
                    image = content.Load<Texture2D>("graphics/crocodile");
                    critterValue = 10;
                    break;
                case Species.HORSE:
                    image = content.Load<Texture2D>("graphics/horse");
                    critterValue = 1;
                    break;
                case Species.ELEPHANT:
                    image = content.Load<Texture2D>("graphics/elephant");
                    critterValue = 50;
                    break;
                default:
                    // This should never happen
                    break;
            }

            click = content.Load<SoundEffect>("audio/buttonclick");
        }
        // ---------------------------
        public void Draw(SpriteBatch spriteBatch)
        {
            if (alive == true)
            {
                spriteBatch.Draw(image, position, Color.White);
            }
        }
        // ---------------------------
        public void Spawn(GameWindow window)
        {
            // Set the critter to be alive
            alive = true;

            // Determine bounds for random location of the critter
            int positionYMin = 0;
            int positionXMin = 0;
            int positionYMax = window.ClientBounds.Height - image.Height;
            int positionXMax = window.ClientBounds.Width - image.Width;

            // Generate random location of the critter within the bounds
            Random rand = new Random();
            position.X = rand.Next(positionXMin, positionXMax);
            position.Y = rand.Next(positionYMin, positionYMax);
        }
        // ---------------------------
        public void Despawn()
        {
            // Set critter to not alive in order to make it not draw and not clickable
            alive = false;
        }
        // ---------------------------
        public void Input()
        {
            // Get the current status of the mouse
            MouseState currentState = Mouse.GetState();

            // Get the bounding box for the critter
            Rectangle critterBounds = new Rectangle(
                (int)position.X, 
                (int)position.Y, 
                image.Width, 
                image.Height );

            // Check if we have the left button held down over the critter
            // And if the critter is alive
            if (   currentState.LeftButton == ButtonState.Pressed 
                && critterBounds.Contains(currentState.X, currentState.Y)
                && alive == true)
            {
                // We clicked the critter!

                // Play SFX
                click.Play();

                // Despawn the critter
                Despawn();

                // Add to score
                scoreObject.AddScore(critterValue);
            }
        }

    }
}
