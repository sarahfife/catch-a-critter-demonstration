﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace CatchACritter
{
    class Score
    {
        // ---------------------------
        // Data
        // ---------------------------
        int value = 0;
        Vector2 position = new Vector2(10, 10);
        SpriteFont font = null;


        // ---------------------------
        // Behaviour
        // ---------------------------
        public void LoadContent(ContentManager content)
        {
            // Load the font from file so it can be used to draw text
            font = content.Load<SpriteFont>("fonts/mainFont");
        }
        // ---------------------------
        public void AddScore(int toAdd)
        {
            // Adds the provided number to our current score value
            value += toAdd;
        }
        // ---------------------------
        public void Draw(SpriteBatch spriteBatch)
        {
            // Draw the score to the screen using the font variable
            spriteBatch.DrawString(
                font, 
                "Score: " + value.ToString(), 
                position, 
                Color.White);
        }
        // ---------------------------
        public void ResetScore()
        {
            value = 0;
        }
        // ---------------------------
    }
}
